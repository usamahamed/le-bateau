<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0040)http://www.iamrakesh.com/demo/le-bateau/ -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Website Vertical Scrolling with jQuery</title>
        
        <meta name="description" content="Website Vertical Scrolling with jQuery">
        <meta name="keywords" content="jquery, vertical, scrolling, scroll, smooth">
        <link rel="stylesheet" href="src/style_vertical.css" type="text/css" media="screen">
    <style>
        a{
            color:#fff;
            text-decoration:none;
        }
        a:hover{
            text-decoration:underline;
        }
        span.reference{
            position:fixed;
            left:10px;
            bottom:10px;
            font-size:13px;
            font-weight:bold;
        }
        span.reference a{
            color:#fff;
            text-shadow:1px 1px 1px #FFF;
            padding-right:20px;
        }
        span.reference a:hover{
            color:#ddd;
            text-decoration:none;
        }
		.section a{
			display:block;
			line-height:90px;
			text-align:left;
			font-size:0px;
		}
		.white{
			background:none !important;
		}

    </style></head>
    
    <body style="background:url(bg.png) center top fixed !important;">
        
        <div class="section" id="section1" style="height:0px !important; float:none;">
               <div style="text-align:center; overflow:hidden; display:block; position:fixed; height:137px; top:107px; width:100%; margin:0 auto; background:url(images/headerbg.png) repeat-x;margin: -107px auto 0;">
               <div style="position:relative; width:1050px; margin:0 auto;">
               <div style="position:absolute; left:0; top:30px; width: 154px; height:70px;"><a href="#section1">1</a></div>
               <div style="position:absolute; left: 155px; top:30px; width: 100px; height:70px;"><a href="#section3">3</a></div>
               <div style="position:absolute; left: 255px; top:30px; width: 120px; height:70px;"><a href="#section5">5</a></div>
               <div style="position:absolute; left: 377px; top:30px; width: 292px; height:7100px;"><a href="#section1">1</a></div>
               <div style="position:absolute; left: 672px; top:30px; width: 230px; height:70px;"><a href="#section4">4</a></div>
               <div style="position:absolute; left: 900px; top:30px; width: 120px; height:70px;"><a href="#section6">6</a></div>
               </div>
               <img src="src/1.png">
               </div>
        </div>
        <div class="section" id="section1" style="height:590px !important;">
        <div style="text-align:center; overflow:hidden; display:block; background:#e8e8e8; width:100%; margin-top:109px; float:none;">
        <embed width="1250" height="500" name="plugin" src="src/header.swf" type="application/x-shockwave-flash" id="flashfirebug_1357756076620" allowscriptaccess="always" allowfullscreen="true" allownetworking="all">
        </div>
        
        <div class="section white" id="section2">
            <div style="text-align:center; overflow:hidden; display:block; background:#e8e8e8; width:100%;"><img src="src/2.png"></div>
        </div>
        <div class="section" id="section3">
        	<div style="padding-top:107px;"></div>
            <div style="text-align:center; overflow:hidden; display:block; width:100%; background:url(images/product-bg.png) repeat-x;"><img src="src/3.png"></div>
        </div>
        
        <div class="section" id="section5">
        	<div style="padding-top:107px;"></div>
            <div style="text-align:center; overflow:hidden; display:block; width:100%; background:#fff;"><img src="src/5.png"></div>
        </div>
        
        <div class="section" id="section4">
        	<div style="padding-top:107px;"></div>
            <div style="text-align:center; overflow:hidden; display:block; width:100%; background:#fff;"><img src="src/4.png"></div>
        </div>

        <!-- The JavaScript -->
        <script type="text/javascript" src="src/jquery.min.js"></script>		
        <script type="text/javascript" src="src/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.section a').bind('click',function(event){
                    var $anchor = $(this);
                    
                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top
                    }, 2500,'easeInOutExpo');
                    /*
                    if you don't want to use the easing effects:
                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top
                    }, 1000);
                    */
                    event.preventDefault();
                });
            });
        </script>
    
</div></body></html>